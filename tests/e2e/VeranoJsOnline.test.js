
describe("VeranoJS Online", () => {
  it("execute code's tests", () => {
    cy.visit("/")
    expectToBeInCodePage()

    runTestByShortcut()
    expectToBeInTestPage()

    backToCodeByClick()
    expectToBeInCodePage()

    runTestByClick()
    expectToBeInTestPage()

    backToCodeByShortcut()
    expectToBeInCodePage()
  })

  it("resets the current code", () => {
    cy.visit("/")
    removeAllTheCodeInEditor()

    resetCode()

    expectToCodeHasBeenReset()
  })

  it("loads the code from a file", () => {
    cy.visit("/")

    cy.get("#code-file").selectFile('tests/fixtures/someJsCode.js', {force: true})

    cy.contains('File content')
  })

  function removeAllTheCodeInEditor() {
    cy.get('#code-editor')
      .click()
      .focused()
      .type('{ctrl}a')
      .type(' ')
  }

  function resetCode() {
    cy.get("#reset-code").click()
    cy.on('window:confirm', () => true)
  }

  function backToCodeByClick() {
    cy.get("#back-to-code").click()
  }

  function backToCodeByShortcut() {
    cy.get("body").type('{backspace}')
  }

  function runTestByClick() {
    cy.get("#run-tests").click()
  }

  function runTestByShortcut() {
    cy.get("body").type('{alt+enter}')
  }

  function expectToBeInCodePage() {
    cy.contains('expect(action).toBe("Fill me correctly")')
  }

  function expectToCodeHasBeenReset() {
    cy.contains('expect(action).toBe("Fill me correctly")')
  }

  function expectToBeInTestPage() {
    cy.contains('Test failed: 1')
  }
})
