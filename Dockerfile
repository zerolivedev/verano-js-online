FROM node:18.12.1-slim
ENV WORKPATH /opt/verano/
ENV USERNAME node
WORKDIR $WORKPATH

RUN chown -R $USERNAME $WORKPATH

USER node
RUN npm config set registry "http://registry.npmjs.org"
COPY --chown=$USERNAME:$USERNAME package* $WORKPATH
RUN npm install --only=prod --no-progress

CMD ["npm", "run", "serve"]

RUN npm install --no-progress
COPY --chown=$USERNAME:$USERNAME . $WORKPATH

VOLUME "${WORKPATH}/node_modules/"
