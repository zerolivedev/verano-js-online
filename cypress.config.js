const { defineConfig } = require('cypress')

module.exports = defineConfig({
  e2e: {
    supportFile: false,
    specPattern: "tests/e2e/*test.js",
    video: false
  }
})
