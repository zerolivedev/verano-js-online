
const goToCodePage = () => {
  document.location.pathname = document.location.pathname.replace("verano.html", "")
}

const goToCodePageOnPressBackspace = () => {
  window.addEventListener("keydown", (event) => {
      if (event.key == "Backspace"){
          goToCodePage()
      }
  })
}
goToCodePageOnPressBackspace()

document.querySelector("#back-to-code").addEventListener('click', (event) => {
  event.preventDefault()

  goToCodePage()
})

const storedCode = localStorage.getItem('code')
if (storedCode && storedCode !== 'null') {
    eval(storedCode)
}
