
class Cursor {
  static savePosition() {
    const position = editor.getPosition()
    const positionAsString = JSON.stringify(position)
    localStorage.setItem('editorPosition', positionAsString)
  }

  static goToPreviousPosition() {
    const positionAsString = localStorage.getItem('editorPosition')
    const position = JSON.parse(positionAsString)

    if (position) {
      this.#goTo(position)
    }
  }

  static goToFirstLine() {
    const position = { lineNumber: 1, column: 1 }

    this.#goTo(position)
  }

  static #goTo(position) {
    editor.setPosition(position)
    editor.revealLineInCenter(position.lineNumber)
    editor.focus()
  }
}
