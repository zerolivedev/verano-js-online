
class CodeRepository {
  REPO_KEY = "code"
  constructor(defaultCode) {
    if (!this.#areThereCode()) {
      this.store(defaultCode)
    }
  }

  store(code) {
    localStorage.setItem(this.REPO_KEY, code)
  }

  retrieve() {
    return localStorage.getItem(this.REPO_KEY)
  }

  #areThereCode() {
    return !!this.retrieve()
  }
}
