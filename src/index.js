const DEFAULT_CODE = 'describe("Subject under test", () => {\n  it("action to test", () => {\n    // Arrange\n    const aNumber = 1\n    const anotherNumber = 2\n    // Action to test\n    const action = aNumber + anotherNumber\n    // Assert\n    expect(action).toBe("Fill me correctly")\n  })\n})'
const codeRepository = new CodeRepository(DEFAULT_CODE)

var editor = monaco.editor.create(document.getElementById('code-editor'), {
  value: codeRepository.retrieve(),
  language: 'javascript',
  fontSize: "24px",
  theme: "vs-dark"
});

const runTests = () => {
  const code = window.editor.getValue()
  codeRepository.store(code)

  Cursor.savePosition()

  window.location.pathname += "verano.html"
}

const resetCodeToExample = () => {
  document.querySelector("#reset-code").addEventListener("click", (_event) => {
    if (confirm("Are you sure? You gonna to trash you code.")) {
      codeRepository.store(DEFAULT_CODE)
      Cursor.goToFirstLine()
      monaco.editor.getModels()[0].setValue(codeRepository.retrieve())
    }
  })
}
resetCodeToExample()

const runTestOnClickButton = () => {
  document.querySelector("#run-tests").addEventListener("click", (_event) => {
    runTests()
  })
}
runTestOnClickButton()

const runTestsWhenIsPressedAltEnter = () => {
  window.addEventListener("keydown", (event) => {
    if (event.key == "Enter" && event.altKey){
      runTests()
    }
  })
}
runTestsWhenIsPressedAltEnter()

const onUploadFileFillEditor = () => {
  const control = document.querySelector("#code-file")
  control.addEventListener("change", function(event){
      const reader = new FileReader();
      reader.onload = function(event){
          const contents = event.target.result;
          editor.setValue(contents)
          control.value = ''
      };
      reader.readAsText(control.files[0]);
  }, false);
}

const onSaveDownloadCode = () => {
  document.getElementById('save-code').
  addEventListener('click', _event => {
    const content = window.editor.getValue()

    if (content) {
      downloadFile('VeranoJSOneline.js', content)
    }
  })
}

let isControlPressed = false
const disableSavePageShortcut = () => {
  window.addEventListener('keydown', (event) => {
    if (event.key === "Control") {
      event.preventDefault()
      isControlPressed = true
    }

    if (event.key === "s" && isControlPressed){
      event.preventDefault()
      runTests()
    }
  })

  window.addEventListener('keyup', (event) => {
    if (event.key === "Control") {
      isControlPressed = false
    }
  })
}
disableSavePageShortcut()


window.addEventListener('load', () => {
  Cursor.goToPreviousPosition()

  onUploadFileFillEditor()
  onSaveDownloadCode()
})
