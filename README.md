# VeranoJS Online

## System requirements

- `Docker version 20.10.18` or compatible.
- `docker-compose  version v2.3.3` or compatible.

## How to run the project?

- You only have to run the following script: `sh scripts/up-d.sh`
- Visit with your browser the URL: `http:localhost:8080`

> Once you end your work use `sh scripts/down.sh`

## Libraries installed via AMD

- monaco-editor version 0.34.0
- VeranoJS  version 1.3.2

> Asynchronous module definition compatible (AMD)

## CI/CD

The project is deployed in `https://zerolivedev.gitlab.io/verano-js-online`
